# Overview

Code for September 2020 workshop on ingress

# Plan

* Simple Demo
  * Create a namespace for `ingress-demo`.
  * Deploy `infocontainer` to `ingress-demo`
  * Create a CNAME in TCB Tech
  * Check results
  * Setup Ingress yaml
* Lemonade Demo on GKE
  * Review existing services
  * Create ingress
  * Create DNS `A` and `CNAME` records
* Repeat Lemonade Demo on AWS
  * Review existing services
  * Create ingress
  * Create DNS `A` and `CNAME` records

# Namespace

```
kubectl apply -f k8s/ingress-demo.yaml`
kubectl get namespaces
```

# Infocontainer Deployment + Ingress

## Container

```
cd infocontainer 
go build .
cd ..
export TAG=infocontainer-$(git log -1 --pretty=format:"%h")
export REG=registry.gitlab.com/eastside-kubernetes-workshop/workshop-ingress-demo
echo ${REG}:${TAG}
docker build -t ${REG}:${TAG} .
docker push ${REG}:${TAG}
```

Test the container wtih

```
docker run -p 8080:8080 ${REG}:${TAG}

```

## Kubernetes

### Option 1: Kubernetes deployment with jsonnet

(Be sure you have [`jsonnet`](https://jsonnet.org/) installed.)

```
jsonnet k8s/infocontainer-deployment.jsonnet -A repository=${REG} -A image=${TAG} | kubectl apply -f -
kubectl apply -f k8s/infocontainer-service.yaml
kubectl get pods -n ingress-demo
```


### Option 2: Kubernetes deployment via manual edit

edit the deployment yaml file to change the continaer name to 
match `${REG}` and `${TAG}`.

```
kubectl apply -f k8s/infocontainer-deployment.yaml
kubectl apply -f k8s/infocontainer-service.yaml
kubectl get pods -n ingress-demo
```

Look at the service via the GKE IP address, finding it with the
Google GKE console.

http://34.121.111.87/


## DNS

Using a DNS provider, Linode in my case, create a `A` record
for the IP address above.

```
nslookup infocontainer-service.tcbtech.info
curl infocontainer-service.tcbtech.info
```

## Ingress

Yes, you have been waiting a long time for this

```
kubectl apply -f k8s/infocontainer-ingress.yaml
kubectl get ingresses infocontainer-ingress --output yaml
``` 


## DNS for Ingress

Create an `A` record for infocontainer-ingress.tcbtech.info

```
nslookup infocontainer-ingress.tcbtech.info
curl infocontainer-ingress.tcbtech.info
```

See Notes on [DNS](DNS-Readme.md)

# Lemonade Demo Ingress

Cup Services is exposed on IP address 35.232.89.228

http://35.232.89.228/cup/small

# Ingress

This ingress yaml file belongs in the lemonade-demo git project, in the
overview project. But it is here for the purposes of our demonstration.

```
kubectl apply -f k8s-lemonade-demo/lemonade-demo-ingress.yaml
kubectl get ingresses lemonade-demo-ingress --output yaml
``` 

http://34.98.115.82/


# Setup `A` Record

```
nslookup lemonade-demo.tcbtech.info
curl lemonade-demo.tcbtech.info
```

# Setup `CNAME` Records

```
nslookup cup.goog.tcbtech.info
curl cup.goog.tcbtech.info
curl cup.goog.tcbtech.info/cup/small

nslookup lemonade.goog.tcbtech.info
curl lemonade.goog.tcbtech.info
curl lemonade.goog.tcbtech.info/lemonade/12

nslookup beverage.goog.tcbtech.info
curl beverage.goog.tcbtech.info
curl beverage.goog.tcbtech.info/beverage/small
```

