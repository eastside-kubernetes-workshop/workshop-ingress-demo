//
// jsonnet infocontainer-deploy.jsonnet -A repository=${REG} -A image=${TAG}
//
function(repository, image) {
    apiVersion: "apps/v1",
    kind: "Deployment",
    metadata: {
	name: "infocontainer",
	namespace: "ingress-demo",
    },
    spec: {
	selector: { 
	    matchLabels: {
		app: "infocontainer",
		deploy: "infocontainer-deploy",
	    },
	},
	replicas: 1, # tells deployment to run only 1 pod
	template: {
	    // create pods using pod definition in this template
	    metadata: {
		labels: {
		    app: "infocontainer",
		    deploy: "infocontainer-deploy",
		    shard: "main",
		},
	    },
	    spec: {
		containers: [
		    {
			name: "infocontainer-deploy",
			imagePullPolicy: "Always",
			image: '%s:%s' % [repository, image],
			ports: [
			    {
				containerPort: 8080,
			    },
			],
			env: [
			    {
				name: "container-tag",
				value: image,
			    },
			    {
				name: "namespace",
				valueFrom:  {
				    fieldRef: {
					fieldPath: "metadata.namespace",
				    },
				},
			    },
/*
* available in a later version of k8s
			    {
				name: "annotations",
				valueFrom:  {
				    fieldRef: {
					fieldPath: "metadata.annotations",
				    },
				},
			    },
*/
			],
			resources: {
			    requests: {
				cpu: "50m",
				memory: "100Mi",
			    },
			    limits: {
				cpu: "250m",
				memory: "500Mi",
			    },
			},
			securityContext: {
			    readOnlyRootFilesystem: true,
			},
			volumeMounts: [
			    {
				name: "tmp",
				mountPath: "/tmp",
			    }
			],
		    },
		],
		volumes: [
		    {
			name: "tmp",
			emptyDir: {},
		    }
		],
		imagePullSecrets: [
		    {
			name: "gitlab-markphahn-registry",
		    }
		],
	    }
	}
    }
}

