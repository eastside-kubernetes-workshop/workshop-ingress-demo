


package main

// package main is used for executable
// https://golang.org/pkg/net/http/

import (
    "fmt"
    "log"
    "time"
    "net/http"
    "os/exec"
)

func displayCommand(c string, w http.ResponseWriter) {
    fmt.Fprintf(w, "\nCmd: <code>%s</code>\n", c)

    cmd := exec.Command("sh", "-c", c)
    stdoutStderr, err := cmd.CombinedOutput()
    if err != nil {
        fmt.Fprintf(w, "<blockquote>")
        fmt.Fprintf(w, "command return error")
        fmt.Fprintf(w, "</blockquote>")
	return
    }


    fmt.Fprintf(w, "<pre>\n")
    fmt.Fprintf(w, "%s\n", stdoutStderr)
    fmt.Fprintf(w, "</pre>\n")
}

func infoHandler(w http.ResponseWriter, r *http.Request) {
    log.Printf("A URL: %s", r.URL)
	t := time.Now()
	s := t.String()

    w.Header().Set("Content-Type", "text/html")
    w.WriteHeader(200)

    fmt.Fprintf(w, "<html>")
    fmt.Fprintf(w, "<head>\n")
	fmt.Fprintf(w, "<title>infocontainer: %s</title>\n", s)
    fmt.Fprintf(w, "<style>")
    fmt.Fprintf(w, "code { font-size: 2em; }")
    fmt.Fprintf(w, "pre { font-size: 1.25em; }")
    fmt.Fprintf(w, "</style>\n")
    fmt.Fprintf(w, "</head>")
    fmt.Fprintf(w, "<html>\n")

    fmt.Fprintf(w, "<body>")

	fmt.Fprintf(w, "Handler: %s\n", s)
    fmt.Fprintf(w, "<br/>\n")
    fmt.Fprintf(w, "URL: %s\n", r.URL)
    fmt.Fprintf(w, "<br/>\n")
	fmt.Fprintf(w, "Host: %s\n", r.Host)
    fmt.Fprintf(w, "<br/>\n")
	fmt.Fprintf(w, "Proto: %s\n", r.Proto)
    fmt.Fprintf(w, "<br/>\n")
    fmt.Fprintf(w, "as String: %s\n", r.URL.String())
    fmt.Fprintf(w, "<br/>\n")
    fmt.Fprintf(w, "<br/>\n")

    displayCommand("uname -a", w)
    displayCommand("date", w)
    displayCommand("whoami", w)
	displayCommand("id", w)

    displayCommand("df", w)

    displayCommand("ls -la /var/log", w)
    displayCommand("ls -la /go/src/app", w)
	displayCommand("ls -la /go/src/infocontainer", w)
    displayCommand("ls -la /tmp", w)
    displayCommand("env", w)

    fmt.Fprintf(w, "</body>")
    fmt.Fprintf(w, "</html>")
}

func main() {
     var spec = ":8080"

    // URL pattern strings are described here:
    // https://golang.org/pkg/net/http/#ServeMux

    // tells the http package to call the function "handler" for all inbound requests
    http.HandleFunc("/", infoHandler)

    // https://golang.org/pkg/net/http/#Server.ListenAndServe
    // if ever the webserver returns, it's bad, so it's fatal
    log.Printf("Listen spec: %s", spec);
    log.Fatal(http.ListenAndServe(spec, nil))
}
