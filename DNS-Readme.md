# DNS Trickery

DNS Caching is tricky. ISP's like Comcast have their DNS Servers
intercept NXDOMAIN replies from an up stream DNS server and holds on
to them for a long time (15 to 120 minutes).

Further, various cacheing layers in the Windows OS and the MacOS will
retain old information (like NXDOMAIN or old IP addresses) for very
long periods. 

Below are some recomendations and hard learned debugging tips.

# Use Google's DNS

Set your DNS server to be a Google DNS server, IP address 8.8.8.8. You
may also want to use CloudFlair's IP of 1.1.1.1. 

# DNS over HTTPS

Your browser, either Google Chrome or Firefox, are configured to use
DNS of HTTPS so by default they use a different server for DNS
lookups. Google Chrome's default is close to what the 8.8.8.8 DNS
server will return so that's helpful. 

# Use nslookup for a bit of investigation

The DNS used in this demo for `tcbtech.info` is hosted on Linode. This
command will show if updates have propigated to all of the Lindoe DNS
servers:

```
nslookup infocontainer-service.tcbtech.info ns1.linode.com
nslookup infocontainer-ingress.tcbtech.info ns1.linode.com
```

(The second argument is the DNS server name to use for the lookup.)

This shows what Google's DNS server knows:
```
nslookup infocontainer-service.tcbtech.info 8.8.8.8
nslookup infocontainer-ingress.tcbtech.info 8.8.8.8
```

# Flush your OS's DNS cache

Most of the demo was done on MacOS, and it ocassionaly got horked
up. The command to flush the cache changes with every version of
MacOS. To find the correct command, do this internet search:

```
macos dns flush cache
```

Probably something similar will provide you help for the Windows OS.
