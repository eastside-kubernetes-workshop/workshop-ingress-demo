FROM golang:1.15-alpine

RUN adduser -D appuser
WORKDIR /go/src/infocontainer
USER appuser

COPY . .

RUN go get -d -v ./...
RUN go install -v ./...

CMD ["infocontainer"]
